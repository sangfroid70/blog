---
authors: ["zilkos"]
title: 'Les principaux courants du Jazz au XXe siècle'
date: 2016-07-09
tags: ["Musique", "Jazz"]
---

## Genre musical souvent perçu comme élitiste, parlons Jazz...

> Je réduis volontairement le nombre de liens dans l'article lui-même car pour chaque époque je me force à citer plusieurs noms, qui pourront servir de base pour vos recherches musicales. Bien évidemment, si vous avez des demandes précises, n'hésitez pas à me contacter !

Vous êtes nombreux à m'avoir demandé d'aborder le jazz sur ce blog. C'est un genre musical que j'adore et que j'ai pratiqué durant de nombreuses années et qui ne cesse de m'étonner encore aujourd'hui.
Commençons ce premier article dédié au jazz avec quelques généralités, puis nous allons parcourir l'histoire du XXe siècle pour voir les différents courants de ce genre musical souvent apprécié, parfois redouté, simple et complexe à la fois. Comme je ne savais pas par où commencer cette suite d'article, je me suis dit qu'une présentation globale de l'histoire de ce genre musical était assez généraliste, pas trop complexe, mais assez informatif pour "débuter". Bien évidemment, je ponctuerai tout ceci d'extraits musicaux, car après tout ce qui compte ici, c'est la musique. Je vous conseille de faire une pause dans votre lecture quand un extrait musical se présente.

### _"Le djaz ? C'pour les intellectuels ça nan ?"_

Pour commencer, on va gentiment tordre le coup à une idée reçue : non, le jazz n'est pas une musique dédiée à une élite. Beaucoup de personnes pensent que pour arriver au jazz, il faut faire l'équivalent des douze travaux d'Astérix en ayant un doctorat de mathématiques appliquées, sans quoi, ce genre musical est difficilement appréciable, ou même jouable.

C'est **faux**.

La magie de cette musique et ce pourquoi elle m'attire toujours autant, c'est qu'on ne peut la reproduire, car le jazz est avant tout très lié à l'improvisation. Et c'est certainement là que le jazz est complexe. Il est rigoureux et il est nécessaire d'avoir un champ de connaissances aussi bien théoriques qu'une certaine dose d'imaginaire, de sensibilité, le tout au service de la création qui est souvent éphémère. Un thème de jazz n'est pas uniquement une complexité apparente faites de notes et d'intervalles abscons, un thème est juste une suggestion, une main tendue au service de l'imagination et de l'expressivité. Certes il faut un bagage technique (surtout pour les musiciens) pour en comprendre toutes les subtilités et les constructions, mais l'auditeur lui, peut ne pas connaître tout cela et simplement apprécier la musique telle qu'elle est : un sentiment unique lors d'un instant unique (si ce n'est pas enregistré) et non reproductible.

En bref, le jazz est simplement de la musique, avec des sentiments, des sensibilités, donc comme beaucoup d'autres genre musicaux. Le voir d'une manière mécanique, pour ne pas dire mathématique est à mon sens un frein à l'expressivité. Je ne dis pas qu'il faut en jouer sans connaître sa complexité solfégique ou rythmique, loin de là, je dis juste qu'il faut ouvrir un peu les yeux et ne pas s'enfermer dans des débats techniques à rallonge et simplement apprécier la chose comme elle est : de la musique.

Qui dit jazz, dit swing. Certes pas toujours, mais souvent.

## Le swing, une relation particulière avec le temps

Le swing en jazz peut désigner deux choses:

- Une notion rythmique
- Un courant musical

Nous, on va uniquement parler dans cette partie de la notion rythmique, pour le courant musical qui s'étend sur une vingtaine d'années, ça sera plus loin.

Le swing est ce "balancement" ressenti quand vous écoutez une oeuvre de jazz. Pas dans toute certes, mais dans l'immense majorité.

### Parlons solfège rythmique

Non, ne partez pas en courant, il n'y a rien de complexe et cela peut se résumer en deux mots : **binaire** et **ternaire**.

Nous allons parler rapidement de croches et de noires **en binaire**. Pour que vous compreniez tous, une noire équivaut à deux croches quand on la "découpe". Si une noire est jouée à une vitesse de 60 battements par minute (BPM), vous allez jouer une note toute les secondes. Pour faire la même chose en croche, comme une noire équivaut à deux croches, vous allez jouer une note toute les 0,5 secondes.

Jusqu'ici, rien de bien compliqué, c'est binaire, on divise par deux, ainsi on se retrouve avec des moitiés (croches), des quarts (doubles-croches), des huitièmes (quadruples-croches) et ainsi de suite, c'est ce qu'on appelle le découpage rythmique, et pour être plus précis quand on découpe en plusieurs sous-parties, il s'agit de la **subdivision du temps**.

Sauf que le jazz, c'est ternaire, donc c'est différent (autrement ce n'est pas drôle).

En **ternaire** il n'y a pas deux croches par noire, mais trois. Pour "swinger", on va en jouer que deux (la première et la troisième), mais ces deux croches n'auront pas la même durée. La première croche aura une durée plus longue que la deuxième. La deuxième sera donc plus proche du temps suivant. Une petite illustration en ASCII parce que je suis sympa :

```
| ==> Le temps
§ ==> Croche
Métronome:   1  et   2  et   3  et   4
Binaire:     |   §   |   §   |   §   |   §
Ternaire:    |     § |     § |     § |     §
```

Autant la croche binaire coupe le temps en deux parties égales, autant la deuxième croche ternaire d'un temps est plus sur la fin du temps en cours, donc plus près du temps suivant.

C'est ce décalage qui donne ce sautillement. Pour vous "prouver" ce sautillement, deux extraits musicaux qui sont en fait des morceaux d'accompagnement pour jouer dessus (super pratique ces trucs-là) nommés *"play along"*.

Le premier, pour [un blues binaire](https://www.youtube.com/watch?v=AmAraX3nXTk). Le deuxième, [pour du ternaire](https://www.youtube.com/watch?v=fKlXu7GsmLA). Ecoutez la batterie attentivement. Sur le blues binaire, elle est binaire, tout est bien découpé en moitié, presque robotique. Pour le deuxième, la batterie à l'aide de ballets joue aussi des croches (pas que, ok), mais ternaires ! Il y a un léger décalage de la deuxième croche qui fait un effet de balancement du type: "Ta ta-ta ta ta-ta ta ta-ta"

_"Oui, mais si c'est écrit pareil, pour simplifier la lecture, comment savoir s'il faut jouer en binaire ou en ternaire ?"_

C'est généralement indiqué sur la partition par les mots **shuffle** ou **swing** ou tout simplement par un symbole comme celui-ci en haut de la partition : ![](/static/img/musique/croches-swing.jpg)

En ternaire, il est possible de jouer des croches binaires, c'est un duolet, et en binaire pour jouer des corches ternaires, c'est un triolet.

Cette précision étant faite uniquement dans un but culturel, passons maintenant à l'évolution du jazz à travers le XXe siècle...

## Origines du jazz

Le jazz est né au début du XXe siècle et est issu de la culture noire-américaine et directement inspiré de trois courants musicaux du XIXe, à savoir le _Negro spiritual_, le _blues_ et le _ragtime_. Ces trois courants musicaux vont poser les fondations de ce que sera le jazz quelques années plus tard. Voyons voir ces trois origines:

### Negro spiritual

Le negro spiritual est avant tout une forme d'expression vocale d'un peuple qui n'avait pas une vie très heureuse, les esclaves noirs américains, principalement venus d'Afrique (contre leur gré bien évidemment). Pour rappel, entre le XVIIe siècle et le XIXe siècle, deux millions d'esclaves venant d'Afrique sont déportés dans les colonies d'Amérique du Nord pour y travailler (principalement pour la culture du coton). C'est la guerre de Sécession qui marquera la fin de l'esclavage avec son abolition le 18 décembre 1865 par Lincoln (la date de l'abolition est variables selon les états et pays).

Et bien ce sont ces esclaves qui sont à l'origine du courant negro spiritual. Il s'agit d'un type de musique vocal uniquement, principal moyen pour eux de s'exprimer. Le simple fait de construire des instruments même rudimentaires était puni par des coups de fouet voir pire, dans l'unique but de ne pas créer de communauté pour éviter tout débordement possible desdits esclaves. Le Gospel est né du negro spiritual en y incluant un côté religieux (christianisme), on y retrouve de nombreux traits communs (outre le côté vocal).

Ce que j'ai trouvé sur Youtube qui s'en rapproche le plus du negro spiritual est [ceci](https://www.youtube.com/watch?v=4G5KtQynWvc&list=PLKdA7sA6arnWD6nIx0nUMfxKoYG6O4Pgd), désolé mais la qualité sonore n'est pas au rendez-vous.

### Le blues

Direction le Mississippi, là aussi à la fin du XIXe siècle et comme pour le negro spiritual, le blues nait dans des champs de coton de part les esclaves là-encore. Initialement vocal, il sera par la suite accompagné d'instruments rudimentaires. On y raconte les déboires de la vie et les conditions difficiles de cette époque. On y trouve un rythme ternaire, une harmonie en I-IV-V (pour ceux connaissant) et... les notes bleues si caractéristiques du blues.

*"Les quoi ?"*

Une note bleue, généralement dite "bluenote" principalement utilisée dans le jazz et le blues, est une note légèrement abaissée, généralement d'un demi-ton, exprimant la nostalgie, la tristesse. On trouve l'origine de ces notes bleues dans le système musical africain. En effet, ils n'utilisaient pas le même système musical que notre **système tonal européen**, le leur est basé sur une **échelle pentatonique** constituée comme son nom l'indique de cinq hauteurs différentes (fa\#, sol\#, la\#, do\# et ré\#). Le système tonal européen comporte lui **sept degrés**. Je ne vais volontairement pas plus loin dans ces explications, car ça peut être très long et assez complexe, de plus ce n'est pas tant le sujet ici. En bref, la rencontre de ces deux systèmes musicaux a fait voir le jour à une adaptation des degrés de notre système tonal: les degrés 3, 5 et 7 ont été légèrement abaissés et c'est là que le blues s'exprime sur des notes de tristesse et de complainte.

En référence madame ["Ma" Rainey](https://www.youtube.com/watch?v=ieEN44N0PZ0), aussi nommée *"The mother of Blues"*.

### Le ragtime

S'il n'y a qu'un nom à retenir, c'est celui de Scott Joplin. Il fut l'un des plus grands compositeurs de ragtime à l'époque où ce genre musical était en plein essor, entre 1870 et 1930. Principalement composé pour le piano, le ragtime ne laisse pas de place à l'improvisation. Généralement très rythmé et avec une basse constante, c'est un réel précurseur du jazz. Dans les années 1930 le jazz a pris le pas sur le ragtime même s'il n'est pas rare d'en croiser ça et là.

Le ragtime est très facilement trouvable un peu partout, Claude Bolling notamment en a fait beaucoup. La référence utilisée ici est ["The Entertainer"](https://www.youtube.com/watch?v=fPmruHc4S9Q) de 1902 par Scott Joplin. Alors non, ce n'est pas qu'une musique de pub pour pâté pour chat, hein... Merci de penser au grand Charlie Chaplin.  

Basiquement le ragtime au piano est vu comme ceci:
  - Main gauche: Basse + accord, d'où les déplacements fréquents et une certaines virtuosité
  - Mélodie à la main droite.

C'est un peu réducteur certes, mais dans le coup on trouve des mariages aussi jolis que bizarres, comme des [covers de Tétris](https://www.youtube.com/watch?v=ZSLnkyPSlEM) version Ragtime ou des choses [bien plus sérieuses](https://www.youtube.com/watch?v=5meWI3iX1sE) (oui, je ne savais pas ou caler Postmodern Jukebox dans un article donc je le pose là).

## On mélange

Vous prenez ces trois genres musicaux issus d'une culture et d'un peuple rêvant de liberté, vous rajoutez quelques pincées de folies, des conditions politiques et de ségrégations sévères et vous avez du jazz...

Le jazz issu de ces mouvements devient populaire vers 1910, dans une ville que tout le monde connaît, au moins de nom...

### Le jazz New Orleans (1920)

La Nouvelle Orléans. Les premières formations de jazz voient le jour dans les rues, sous forme de fanfares, principalement composées de cuivres et uniquement joué par la communauté noire. Ce courant musical mélange ses origines musicales présentées plus haut ainsi que la culture des Antilles françaises. Les cuivres sont omniprésents et très caractéristiques du New Orleans, il s'en dégage notamment un style très particulier pour le trombone à coulisse appelé _tailgate_ qui entre en collaboration rythmique avec la trompette. Le jeu de Kid Ory illustre extrêmement bien ce style, grand tromboniste de l'époque.

Ce style de jazz connaîtra un renouveau dans les années 1940.

Parmi les musiciens les plus connus de ces deux époques, on notera Louis Amstrong, Sydney Bechet, Thelonious Monk, Art Tatum, Paul Barbarin.

[Cette référence](https://www.youtube.com/watch?v=oY3Go5Ydeao), bien qu'elle date de 1947 (l'enregistrement) illustre très bien le jazz New Orleans avec une prédominance des cuivres.

Les blancs, probablement jaloux, ont eux aussi joué du New Orléans, mais il est nommé le **Dixieland**, on trouve aussi le terme de Jazz Hot.

### Le swing (1930)

Quelques années plus tard, c'est la panique avec le krach boursier de 1929 et le peuple a un grand besoin de divertissement. Certains ont des idées, notamment un certain Benny Goodman qui allie la danse et le jazz sur des rythmes endiablés, le swing était né. Avec sa naissance, les formations s'agrandissent pour laisser place à des Big Bands contenant des solistes pour garder l'improvisation typique du jazz. Le swing fut un vrai succès dans ces années qui aidaient à supporter "la grande dépression" allant de 1929 à la deuxième guerre mondiale. La radio, aidait à la diffusion de ce genre musical. Et bien sûr, nombreux était les endroits nocturnes, véritables boites de nuit de l'époque où les danseurs se mêlaient aux musiciens sur fond d'alcool dans l'arrière-boutique (prohibition).

Le swing, le vrai, [de monsieur Louis Prima (1935)](https://www.youtube.com/watch?v=r2S1I_ien6A) va clairement vous faire onduler de l'arrière-train (si vous me permettez l'expression).

Parmi les plus grands, Benny Goodman, Count Basie, Duke Elligton, Coleman Hawkins, la si merveilleuse [Ella Fitzgerald](https://www.youtube.com/watch?v=PrVu9WKs498), les monuments de la batterie que sont Jo Jones et  Gene Krupa. Ha et aussi un guitariste... un certain Django Reinhardt, Jean de son véritable prénom.

_" Comment ça Django Reinhardt ?!"_

Et oui, les français (bien que Django était d'origine belge) qui à l'époque étaient plus orientés bal musette se sont inspirés aussi des big bands des années 30 pour faire éclore le jazz manouche aussi appelé swing manouche. Le jazz manouche tire ses inspirations des musiques gitanes mais aussi d'Europe centrale, comme le [Klezmer](https://www.youtube.com/watch?v=jOKnUKIZ_Kc), de la musette et du Jazz. Sachez que je voue un amour intersidéral à la musique Klezmer. Les formations de base de l'époque sont principalement composées de deux guitares rythmiques (dont un qui fait office de soliste), d'une contrebasse et d'un violon (Stéphane Grappelli étant l'un des plus grands de l'époque).

Aujourd'hui, le monument du jazz manouche connu de tous est [Angelo Debarre](https://www.youtube.com/watch?v=uQPj6Zw4nTc) (excusez le contrebassiste qui est un peu à la bourre au début). Bien évidemment ils sont nombreux et très talentueux, les Rosenberg, Biréli Lagrène, Romane, Dorado et Tchavolo Schmitt, Lucien Moreno, etc.


### Le jazz Bebop (1940)

Dans les années 1940, certains souhaitent complexifier un peu les choses, changer les harmonies pour amener une peu de dissonance et modifier les rythmiques. Pourquoi un tel changement ? Ce changement vient en premier lieu des afro-américains qui étaient las de la discipline omniprésente des big bands (moins de 100 ans avant, les afro-américains étaient des esclaves, n'oubliez pas). Ils souhaitaient se faire plaisir et se libérer. Ce sont donc des formations de musiciens plus réduites où le maître mot est liberté. Liberté, aussi bien dans l'interprétation que dans l'improvisation qui y est beaucoup plus présente. Ce mot "liberté" raisonne aussi quand il y a autant ségrégations raciale.

Le bebop est joué, avant d'être écrit, principalement par certains grands noms comme Charlie Parker, Thelonious Monk, Dizzy Gillepsie, Kenny Clarke et bien d'autres. C'est une véritable opulence musicale, avec des tempos très rapides, des grilles complexes avec des accords changeant très souvent. Par conséquent, le jazz bebop impose une réelle technique de l'instrument de part les tempos élevés et une grande connaissance musicale de part la complexité harmonique des oeuvres.

Ce genre a fait couler beaucoup d'encre, même en France entre les défenseurs du "jazz pur" pour qui le bebop n'était pas du jazz et pour les personnes plus "ouvertes" qui considéraient le bebop comme du jazz.

Le bebop est un véritable mouvement de réaction face au swing et ses gros orchestres où la rigueur est de mise.

Comme référence, le grand [Dizzy Gillepsie](https://www.youtube.com/watch?v=09BB1pci8_o), attention c'est **rapide**, vraiment (supérieur à 250 BPM).

### Le jazz cool (1950)

Tout comme le bebop est une réaction au swing, le cool est une réaction au bebop !

Vrai courant ou non, le jazz cool n'a rien à voir avec le bebop. On y retrouve une ambiance beaucoup plus calme, des sonorités moins "tendues" harmoniquement. Le tout étant beaucoup plus sobre, moins virtuose, avec des sons doux et feutrés. L'incontournable dans ce genre de jazz est l'immense Miles Davis, on peut également cité le saxophoniste Lester Young et bien sûr Gerry Mulligan et son quartet. Ce jazz cool se déplacera vers la cote ouest où il sera joué principalement par des musiciens blancs. Un album de Miles Davis nommé "Birth of the Cool" représente bien ce qu'est le jazz cool (même si ce n'est pas lui qui a choisi le titre).

Les noms à retenir pour ce genre: Miles Davis, Chet Baker, Gerry Mulligan, Dave Brubeck, Gil Evans.

Niveau référence ça peut être chargé, je me limite à deux incontournables, [So What](https://www.youtube.com/watch?v=DEC8nqT6Rrk) (de Miles Davis hein, pas de Pink soyons sérieux) et le célébrissime [Take Five](https://www.youtube.com/watch?v=SVGotpIxkGU) de Dave Brubeck Quartet. Notez d'ailleurs que Take Five est avec une mesure à 5 temps, c'est l'un des premiers à intégrer ça dans le jazz "grand public", et avouons-le, même si ça met quelques noeuds au cerveau au début, c'est très efficace. Rien à voir, mais notez la classe internationnale dont faisait preuves les musiciens sur scène.

Et trois ans plus tard, la réponse de la communauté afro-américaine ne se fait pas attendre avec le...

### Le Hard Bop (1953)

Véritable retour aux sources de la musique noire, le hard bop mélange le blues, le gospel et est une réelle réponse "aux blancs" qui jouent du cool jazz et qui parfois font preuve de racisme (d'accord trop souvent). Concrètement, le hard bop garde les complexités harmoniques du bebop mais ralentit un peu le tempo. C'est aussi l'avènement d'une formation qui est encore beaucoup utilisée de nos jours: le quintet. Les quintets de l'époque étaient composés d'une base rythmique avec batteur, pianiste et bassiste (ou contre-bassiste plutôt) et deux instruments à vents, généralement une trompette et un saxophone. Ils jouent un thème puis partent sur un cycle d'improvisation ou chacun fait un solo improvisé.

Les grands de ce genre et je commence volontairement par les batteurs car ils ont apporté beaucoup de choses au hard bop: Max Roach, Art Blakey, Philly "Papa" Joe Jones, Sonny Rollins, Miles Davis. A noté que le batteur Art Blakey créera à cette époque une formation mythique: Art Blakey and the Jazz Messengers.

Et enfin, pour finir cet historique, nous allons aborder un genre de jazz bien méconnu et généralement peu apprécié...

### Le Free Jazz (Fin 1950)

Là aussi c'est une histoire d'affirmation des racines profondes de la musique noire. Radical, le Free Jazz de l'époque ne retient que quelques éléments cruciaux, fondateurs et typique de la musique noire :

- Énergie
- Sons bruts
- Improvisation

Plus de thème, plus de rythme, plus d'harmonie, plus de grille. Juste une improvisation collective, de l'énergie brute et les instruments sont parfois poussés au bord de leur limite technique.

Ce fut très mal perçu et très décrié, autant dire que le public est clairement réduit en ce qui concerne le free jazz et pourtant c'est un genre de jazz comme un autre, même si très différent. Beaucoup le caractérisent comme "du bruit" où "tout le monde fait n'importe quoi". Sachez que non, le free jazz n'est pas n'importe quoi. C'est structuré par la pensée et par le ressenti de chacun, l'un nourrissant l'autre. J'en conviens, c'est très particulier et relativement rebutant pour ne pas dire "dur à écouter" sans connaissance des possibilités intrumentales.

C'est grâce au free jazz que j'ai multiplié par 5 le nombre de sons possibles avec une batterie courante (dont certains qui sont vraiment inattendus).

Les grands du genre sont surtout Ornette Coleman, John Coltrane, Charles Mingus, Sun Ra et d'autres.

Bien évidemment ce mouvement représente une volonté de libération culturelle pour les noirs américains. Un exemple avec [Ornette Coleman](https://www.youtube.com/watch?v=xbZIiom9rDA) (même si celui-ci reste un peu structuré et calé rythmiquement, attention ça peut surprendre).

## Après 1960

Au-delà de 1960 on assiste surtout à des fusions entre des courants musicaux existants et le jazz. Comme le latin jazz qui cumule la musique latine et le jazz, le jazz rock et bien d'autres courants assez éloignés du jazz tel qu'on vient de décrire.

Alors attention, tout n'est pas à jeter **loin de là** ! Nous avons des merveilles de jazz fusion avec des personnalités incontournables, comme [Herbie Hancock](https://www.youtube.com/watch?v=XrgP1u5YWEg) notamment. C'est juste que cet article fait un simple état des lieux **historique** de cette musique assez récente, mais déjà immensément vaste et se concentre sur les formes principales de jazz admises de tous.

De nos jours, on peut aussi trouver de l'électrojazz ou electroswing et ce depuis le début des années 2000, forme musicale assez en vogue, notamment avec [Caravan Palace](https://www.youtube.com/watch?v=EE7XkaFFtGE), [Parov Stelar](https://www.youtube.com/watch?v=Eco4z98nIQY), [Gramatik](https://www.youtube.com/watch?v=_m2zGncgLjw), [G-Swing](https://www.youtube.com/watch?v=FM09wgvhPjQ) pour les clubers et bien d'autres. Un simple mélange de musique électronique et de jazz (parfois repris, parfois composé) assez efficace.

## Fin

Même si l'article est assez court, de peur de vous noyer dans trop de référence ou de détail, j'espère quand même que vous y voyez un peu plus clair dans l'histoire de cette musique, forgée dans une période sombre de l'Amérique, mais symbole d'une émancipation et d'une quête de Liberté, avec un grand L.

Alors attention, ce n'est pas précis, notamment sur les dates, il manque également des références géographiques, politiques, économique pour bien comprendre pourquoi tel ou tel courant est apprécié ou déprécié, pourquoi il apparait à ce moment et pas un autre, c'est immensément complexe. N'hésitez pas à lire des livres sur le sujet notamment la référence dans le domaine: L'odyssée du jazz de Noël Balen

A bientôt, et que ça swing !
