---
authors: ["zilkos"]
title: "L'accordéon"
date: 2016-05-16
tags: ["Musique", "Accordéon"]
---


## Souvent ringardisé et mal perçu, penchons-nous sur l''accordéon... 

> Les liens présents dans cet article sont précédés ou suivis de _\[DZ\]_ pour une source Deezer et _\[YT\]_ pour une source Youtube. Aussi, je vous conseille de créer un compte Deezer avec [un mail poubelle](http://www.yopmail.com/), car de base vous êtes limités à 30 secondes d'écoute d'un morceau. Ça peut être pratique et la base de données musicale est tout bonnement impressionnante en plus d'être complètement gratuite et à volonté (au moins sur PC). Enfin, sachez que pour votre confort, je n'intègre pas de vidéo ou de bloc Deezer sur ce blog.


Nous ne pouvons pas dire qu'actuellement l'accordéon jouit d'un terrible succès auprès du public non mélomane, pas comme le piano, le violon ou d'autres instruments largement plus plébiscités. Nous allons donc nous pencher sur l'accordéon, sous des formes que vous ne connaissez peut-être pas pour essayer de casser le mythe que de nombreuses personnes tendent à véhiculer via des phrases assassines du genre: _" Ha, boh non, ça joue que du musette c'est tout pourri "_. Ce genre de phrase qui inspire une grande violence envers la personne quand on connaît la richesse et la polyvalence de cet instrument.

Mais d'abord, qu'est-ce que c'est et d'où vient l'accordéon ?

## Un peu d'histoire

D'un point de vue historique, peu de monde est d'accord, comme souvent quand il s'agit d'histoire. Ceci dit tout le monde s'accorde plus ou moins sur le XIXe siècle pour dire que c'est à cette époque qu'il est né dans la forme que l'on connaît. On retrouve un nom souvent cité quand on aborde ce sujet : Cyrill Demian.

Ce monsieur, arménien de naissance a déposé un brevet le 6 mai 1829 concernant un instrument nommé "**Accordion**". Donc oui, c'est un instrument créé pendant la période _Romantique_. On s'accorde plus ou moins à dire que c'est lui l'inventeur de l'accordéon. Du moins, c'est lui qui a rassemblé en un seul instrument les caractéristiques de l'instrument que nous connaissons aujourd'hui. Pour trouver les "prédécesseurs" de l'accordéon, ou tout du moins des instruments qui présentent des caractéristiques communes à l'accordéon, il faut aller en Chine et remonter de 2000 à 3000 ans avant la naissance d'un certain JC pour trouver un instrument nommé _Sheng_, un orgue à bouche disposant d'une tessiture allant de ré3 à fa\#5, le ré3 étant notre ré de la clef de sol.

Le Sheng ressemble à ceci:

{{% center %}}
![Un sheng](/static/img/musique/Sheng.jpg)
{{% /center %}}

On en trouve énormément d'autres, dont le concertina en 1829 (puis amélioré en 1844) et ses multiples déclinaisons, ainsi que le très célèbre bandonéon, qui nous vient d'Allemagne et non pas d'Amérique du Sud, comme beaucoup de monde le pensent. Le bandonéon apparaît un peu après 1830, car il est directement inspiré du concertina. Nous trouvons aussi l'accordina, c'est comme un accordéon, mais à un seul clavier et soufflé à la bouche.

La caractéristique commune de ces instruments, c'est qu'ils sont à vent, mais aussi et surtout, à anche libre. Voyons ce que c'est.

### Point technique: Anche libre et vibration

Une anche est une lame (métallique dans le cas de l'accordéon, en bambou chez les saxophoniste) qui vibre grâce à un flux d'air, la vibration de la lame étant le son produit. C'est tout. On retrouve ce système d'anche dans de nombreux instruments, comme le saxophone, des orgues à bouches, l'harmonica, le hautbois et plein d'autres. Elles sont diverses et variées, les anches peuvent être vibrantes, battantes, contrôlées par la bouche, etc. Comme je suis plutôt sympa et aussi parce que je n'ai pas trouvé d'image correcte d'une anche d'accordéon, j'ai sorti Blender (un logiciel libre de conception 3D) et après ~~deux heures~~ quelques minutes de travail, j'ai sorti quelque chose qui y ressemble (soyez indulgents je ne suis pas infographiste). J'ai volontairement fait ressortir la lamelle de son emplacement de manière un peu plus prononcée que dans la réalité pour mieux voir à quoi cela ressemble (normalement il y a moins d'espace entre la lamelle et son support) :

{{% center %}}
![Une anche (à peu près...)](/static/img/musique/rendu.png)
{{% /center %}}

Pour ceux que ça intéresse, j'ai fait un rendu vidéo où cette pièce tourne à 360° afin de la voir sous tous les angles, [la vidéo est disponible ici (2,93Mo / MPEG2)](/static/img/musique/anche.mpeg), j'ai uniquement testé avec VLC.

L'air fait vibrer la partie centrale (le genre de lamelle) et produit un son, tout simplement. Après tout, le son n'est qu'une vibration de l'air qui se propage via des mécanismes de variation de pression de l'air. C'est cette variation de pression (compression / décompression) qui se déplace grâce aux différents composants de l'air. Lancez un cailloux dans l'eau, vous verrez que l'eau ne se déplace pas, elle reste au même endroit en se cantonnant à un déplacement vertical. Au niveau de la vitesse de déplacement des ondes sonores, comptez 334 mètres par seconde dans de l'air à température ambiante (20°C), donc "en gros" comptez un kilomètre toute les 3 secondes. Oui je sais je fais une digression, mais c'est toujours sympa de connaître ce genre de choses, après tout on entend plein de sons toute la journée, mais il ne s'agit que de vibrations, c'est bien pour ça qu'il n'y a pas de son dans l'espace, car il n'y a pas de matière pour supporter les ondes, donc pas de propagation, donc par conséquent: pas de son.

L'accordéon dispose de deux claviers, nommés clavier d'accompagnement pour la main gauche et clavier de chant pour la main droite. Donc typiquement, la partie soliste (le chant) sera joué à la main droite. Concernant l'air, c'est le soufflet qui s'en occupe. Le soufflet étant la partie centrale, emblème typique de ce que représente l'accordéon (et utilisé dans beaucoup d'expressions françaises comme "plié en accordéon"). Pour actionner le soufflet, il suffit de tirer ou de pousser la main gauche qui, elle seule doit bouger. En parlant d'air, arrêtons-nous sur une différence fondamentale et assez mal connue de tous concernant l'accordéon...


### Point technique: Diatonique ou chromatique ?

Il est souvent admis qu'un accordéon diatonique dispose d'un clavier dit "de piano" alors que le chromatique dispose de touches rondes. Juste oubliez ceci, **c'est d'une bêtise sans nom** et ça n'a aucun rapport avec le sujet. 

Diatonique ou chromatique, c'est une histoire de son (et de manipulation du soufflet).

 - Un accordéon **diatonique** est _bisonore_. En appuyant sur un seul bouton, vous pouvez faire **deux** sons, selon si vous tirez ou poussez le soufflet. Les deux claviers sont construits sur la gamme **diatonique** (7 degrés composée de 5 tons et 2 demi-tons)
 - Un accordéon **chromatique** est _unisonore_. En appuyant sur un seul bouton, vous ferez **un seul** son, que vous poussiez ou tiriez le soufflet. Les deux claviers sont construits sur la gamme **chromatique** (12 degrés).

Donc attention, ce sont deux instruments radicalement différents, car la technique instrumentale n'est pas similaire (hormis le mécanisme des touches).

Qu'il s'agisse de boutons ronds ou de l'équivalent de touche de piano au clavier de chant, c'est uniquement une question de préférence de jeu.

### Popularité naissante

Peu de temps après sa conception l'instrument commençait à être vraiment populaire si bien qu'il s'exporta rapidement en Amérique. À tel point que dans les années 1930 l'accordéon devient l'instrument de prédilection d'un genre musical pas franchement très connu par chez nous et pourtant uniquement chanté en français dans ses débuts, j'ai nommé le Zarico (ou Zydeco _in english_) qui comporte de nombreuses influences venant du blues (et du _Rhythm and blues_).

Par exemple, le morceau [Back Door Man](http://www.deezer.com/track/64757439) (DZ) de Keith Frank issu de l'album _On a mission_ paru le 17 juin 1998. Zarico relativement "moderne" mais ça vous donne un bon aperçu de ce genre musical.

Je ne m'étends pas beaucoup sur le Zarico (ou Zydéco), mais si vous faites quelques recherches dessus, vous allez vite trouver des groupes créoles. Sachez qu'à l'origine le Zarico était chanté en français uniquement, principalement en Louisiane. Pour avoir une idée du Zarico "moins moderne", penchez-vous du côté de Boozoo Chavis qui était considéré comme un pionner dans le domaine (décédé en 2001).


### Fin XIXème et XXème

Clairement, l'accordéon trouve sa raison d'être en France dans un genre musical qui le ringardisera un siècle plus tard... Le musette. Alors non, rien à voir avec le hautbois baroque du même nom. Le musette est directement inspiré d'un folklore d'Auvergne qui a réussi par je ne sais quelle prouesse à être joué à La Bastille à Paris. On y joue principalement de la bourrée, qui sera remplacé au XXe siècle par la valse.

> La bourrée, généralement en deux temps, nous vient d'Auvergne et désigne une danse traditionnelle.

Pour ceux étant jeunes et n'ayant jamais mis les pieds dans un bal musette, sachez qu'on y trouve plusieurs genres musicaux (et si, vous aurez des extraits !). Attention tout de même, dans beaucoup de bal musette il est d'usage d'utiliser des accordéons au son assez criard, que de manière personnelle, je déteste. Aussi, ça ne va pas chercher bien loin musicalement et harmoniquement parlant...

L'incontournable valse musette, qui, si elle est bien maîtrisée, permet éventuellement de ramener la demoiselle ou le damoiseau à domicile. Attention la valse musette est généralement plus rapide et exécutée "en toupie". Pensez donc à votre papa ou maman qui est peut-être le fruit d'une union qui a commencé par une valse musette comme [celle-ci](http://www.deezer.com/track/1043163) (DZ) (de notre bien aimée Yvette Horner). Dans un style très similaire à la valse, on rencontre aussi des javas (qui n'a pas tellement de différence, si ce n'est quelques roulés de caisse claire de la part du batteur pour savoir si il dort ou pas).

Bien évidemment l'endiablé paso-musette, plus simple, mais surtout plus sensuel que le paso-doble pour le plaisir des dames. [Voir ici](http://www.deezer.com/track/119710550) (DZ) pour un aperçu chaleureux de monsieur Verchuren, pointure du musette.

Pour encore plus de chaleur, de sensualité et de rapprochement, le tango-musette. Le tango tout simplement, mais en moins "sportif", tout aussi agréable à l'oreille. Pour le tango, je vous propose deux versions du même morceau, parce que le tango c'est sérieux (parole de ma grand-mère). Je vous parle bien sûr du tango le plus connu de tous les danseurs, la _Cumparsita_. [Première version](http://www.deezer.com/track/5324744) (DZ) "classique", bien trop robotique à mon goût, mais tout de même efficace et faisant fureur dans les bals. [La deuxième version](https://www.youtube.com/watch?v=40SWD7NSjAI) (YT) en vidéo, qui dispose, outre d'un ensemble d'instruments plus agréable, de sonorités bien moins soldées qu'une boucle midi un peu crade. C'est certes pas joué à l'accordéon, mais au bandonéon, en outre, vous avez des danseurs histoire de vous montrer qu'attirer des femelles ou mâles sur des tangos endiablés est toujours d'actualité et dispose d'un raffinement non négligé (bande de sagouins).

Des bals, on en trouvait absolument partout, dans la moindre bourgade de campagne jusque dans les grandes villes. Véritable lien social et divertissement musical de l'époque (et même encore aujourd'hui pour nos ainés), si bien que de multiples groupes et formations de bal émergèrent par centaines, allant de "René et son Orchestre" à quelques accordéonistes virtuoses disposant d'un rayonnement au moins régional, et parfois national. Dans mes jeunes souvenirs de campagnard, il y avait aussi des émissions télévisuelles le dimanche concernant l'accordéon, traumatisme de jeunesse sans doute, mais l'image est restée. Ce genre d'émission, mal montée, au son ravagé (et en playback), clairement tourné vers le passé n'a fait qu'augmenter la ringardisation de ce si bel instrument.

Mais les bals et cette culture populaire ont directement basculés dans le passé. Les générations changent, les "jeunes" se moquent des "vieux" et dénigrent parfois leurs petits plaisirs d’antan et la mode passe...

Sur la fin du XIXe siècle, l'émergence de la musique anglo-saxonne, la montée du rock et les moyens techniques dans le monde de la musique font que le bal est relégué au dernier plan pour laisser place à des endroits barbares, où les humains se transforment en bout de viande suant, malodorant et alcoolisés: les boites de nuit. Sans compter l'émergence et la popularisation de la musique électronique qui finira d'achever le bal musette de nos grands-parents. Mais il résiste encore si bien qu'il est assez facile de trouver un bal actuellement, même si il est plus aisé d'en trouver en campagne.

Beaucoup de personnes ne s’intéressant peu à la musique ont gardé cette image "_bal_ = _accordéon_" et par conséquent "_accordéon_ = _bal_" avec tristesse. Sauf que cher visiteur, l'accordéon ne se réduit pas qu'à ça et bien évidemment.

## Dans la musique dite "classique"

L'accordéon a déjà obtenu ses lettres de noblesse dans la musique dite classique. Il y a de nombreuses pièces pour accordéon dès la fin du XIXe siècle et certains compositeurs l'utilisent déjà pour apporter des sonorités différentes, comme le célèbre Tchaïkovski dans sa _Suite pour orchestre numéro 2_ (op 53) dans laquelle il intégrera 4 accordéons sur le troisième mouvement ([voir ici](https://www.youtube.com/watch?v=ZQ1UuIIqzaE) (YT)).

Bien d'autres compositeurs ont utilisé l'accordéon, comme Prokofiev, Chostakovitch, Badings, Berio et d'autres.

Des œuvres classiques sont également très répandues en Europe de l'Est, mais sont jouées au bayan, un accordéon plus volumineux (105 boutons à la main droite et 120 à la gauche) l'équivalent de l'accordéon de concert chez nous. L'accordéon est extrêmement répandu dans les pays de l'est et notamment très orienté dans la musique dite classique, il y a des soirées de concert et des classes de conservatoire entièrement dédiées à ce seul instrument. [Voyez donc Alexandr Hrustevich](https://www.youtube.com/watch?v=SzA8O-aTOTQ) (YT) interpréter le dernier mouvement des 4 saisons de Vivaldi au bayan. 

Si vous êtes observateurs, vous voyez qu'avec le menton il appuie sur des boutons, ils servent à changer le **registre**. Certains accordéons (les bayans, les accordéons de concert, mais aussi les plus modestes) disposent d'un système d'occultation des voix pour jouer les voix seules ou couplées avec d'autres, on appelle "registre" la combinaison de ces voix. Soit les boutons sont disposées sur le haut de l'instrument et sont actionnés au menton, soit au centre du clavier et actionnés avec les doigts tout simplement (pendant un silence de préférence hein).

Il s'agit donc d'assembler des voix pour avoir des sonorités différentes, pour information, les registres les plus connus et/ou utilisés sont les suivants :

 - Flûte
 - Piccolo
 - Vibrato (flûte + flûte haute)
 - Basson 
 - Bandonéon (flûte + basson)
 - Bando aiguë (piccolo + flûte)
 - Registre 15ème (basson + piccolo)
 - Organ (basson + flûte + piccolo)
 - Plein jeu (toutes les voix ensemble)

C'est très similaire avec le système de son d'un orgue (mais avec carrément moins de possibilité).

Outre dans la musique classique, l'accordéon a été et est très utilisé dans le Jazz dès les années 1920 (période Swing). En France et après cette période, vers la moitié du XXe siècle nous avons encore des accordéonistes Jazz assez célèbre comme Jo Privat (surnommé _Le gitan blanc_) qui a notamment joué avec Django Reinhardt (mais pas que). Un français, qui a donc joué beaucoup de swing, mais aussi des valses musettes que personnellement je trouve immensément plus belles que les standards musette. Au choix, une version de [Minor Swing](http://www.deezer.com/track/124078586) (DZ) qu'on ne présente plus, [Les Yeux Noirs](http://www.deezer.com/track/76109178) (DZ) tout autant si ce n'est plus connu, et enfin une jolie valse avec quelques accents manouches: [Sa préférée](http://www.deezer.com/track/124078548) (DZ).

Il y a d'immenses accordéonistes qui méritent le détour, comme Gus Viseur, Médard Ferrero, Marcel Azzola, et un que j'aime particulièrement: Tony Murena. Il a fait une valse magnifique du nom d'[Indifférence](http://www.deezer.com/track/119057598) (DZ) qui a été reprise de nombreuse fois ([Cafe accordio orchestra](https://www.youtube.com/watch?v=nwXY0II9-Mo)(YT), [Angelo Debarre](https://www.youtube.com/watch?v=A0st5Ikip0k)(YT) et énormément d'autres). Évidemment, il y en a beaucoup d'autres que je passe sous silence, tous les aborder est impossible.

## Aujourd'hui

Aujourd'hui et même un peu avant, l'accordéon est soliste, mais aussi accompagnateur et contre-chant. Très honnêtement, jeune comme moins jeune, qui ne se rappelle pas de "Vesoul" de Jacques Brel avec un Marcel Azzola endiablé ? Pour les plus sceptiques, [voir ici](http://www.deezer.com/track/2266776) (DZ). N'oublions pas [Serge Lama](http://www.deezer.com/track/772384) (DZ) qui a fait une tournée avec pour seul musicien un accordéoniste (Sergio Tomassi). J'ai eu le plaisir de voir un concert comme celui-ci même si à l'époque je n'étais pas spécialement orienté sur ce genre de musique et au final on s’aperçoit qu'un accordéon se suffit à lui-même, surtout quand on connaît le talent de monsieur Lama (et que l'accordéoniste dispose d'un système midi intégré à l'accordéon pour sortir des rythmiques).

Le jazz n'est pas en reste avec un excellent [Marc Berthoumieux](http://www.deezer.com/track/10107085) (DZ), monsieur [René Sopa](http://www.deezer.com/track/12450369) (DZ), [Ludovic Beier](https://www.youtube.com/watch?v=ndXdGcBWB-A) (YT) ainsi que [Frédéric Viale](http://www.deezer.com/track/67717612) (DZ) et énormément d'autres là-aussi.

Mais l'accordéoniste français actuel le plus connu de tous reste Richard Galliano... Une petite sélection par mes soins dans l'immensité que représente son œuvre:

- [\[DZ\] Chat pitre](http://www.deezer.com/track/119476168). Imaginez un chat, timide, mais pitre.
- [\[DZ\] New York Tango](http://www.deezer.com/track/7311183), un tango "urbain".
- [\[DZ\] Libertango](http://www.deezer.com/track/17544893), un tango composé par [\[YT\] Astor Piazzolla](https://www.youtube.com/watch?v=vaXNdVTGT0k), bandonéoniste
- [\[YT\] La valse à Margaux](https://www.youtube.com/watch?v=F52oYAzku00) avec l'excellent Sebastian Surel au violon ([\[YT\] et une version en quartet](https://www.youtube.com/watch?v=-OEKEzw2tWc)) (YT)

De genre moins classique et sans doute plus connu du grand public même si très orienté dans un certain style, nous avons aussi notre lot de personnalités. Dans le désordre:

- [\[YT\] La rue Kétanou](https://www.youtube.com/watch?v=JahdXT7tfUE)
- [\[YT\] Les Têtes Raides](https://www.youtube.com/watch?v=GGqtbu4frNI)
- [\[YT\] Sanseverino](https://www.youtube.com/watch?v=mQ_u5xJzcUQ)
- [\[YT\] Yann Tiersen](https://www.youtube.com/watch?v=Xv-QH999PAQ)
- [\[YT\] Les Hurlements d'Léo](https://www.youtube.com/watch?v=4cLKE_rzg_k)
- [\[DZ\] Yves Jamait](http://www.deezer.com/track/3770580) Une spéciale pour les lecteurs qui pratiquent les #NuitDebout

Enfin en vrac et pour vous faire découvrir des choses, on retrouve les très talentueux accordéonistes du groupe Motion Trio qui n'utilisent **que** des accordéons. C'est simplement magnifique et très expressif, une petite sélection:

- [\[DZ\] Litle Story](http://www.deezer.com/track/2911590) - Ils nous racontent une petite histoire en musique, douce et agréable.
- [\[DZ\] Train to Heaven](http://www.deezer.com/track/2732410) - Ils reproduisent "l'ambiance" d'un train, ni plus ni moins.
- [\[DZ\] Tango](http://www.deezer.com/track/2911591) - Un tango agressif

Enfin et pour terminer dans la liste des choses à écouter à base d'accordéon, sachez qu'[il se marie très bien avec une clarinette](http://www.deezer.com/track/79075378) (DZ), comme vous le prouve quelqu'un que je connais très bien. Il est bien évidemment très efficace pour [les musiques de l'est](http://www.deezer.com/track/79075379) (DZ).

Bien évidemment, nous trouvons de nombreux genres et styles propre à l'accordéon dans quasiment tous les pays du monde, notamment en Amérique du Sud avec le Chamamé d'Argentine, le Forro qui est issu du Brésil, le Cueca qu'on trouve initialement au Chili au début du XXe siècle et qui par la suite deviendra la danse nationale de la Bolivie et de l'Argentine, le Cumbia et le Vallenato en Colombie et au Mexique, etc. Lister tous les styles propres à l'accordéon de tous les pays est une tâche immense et bien évidemment en dehors des propos de cet article, qui se borne simplement à vous montrer toute l'étendue musicale et culturelle de cet instrument. Autrement, il faudrait aussi que je liste toutes les tribus d'Afrique, notamment Zoulou et plus globalement dans la culture basotho (Afrique du Sud) où le concertina puis l'accordéon ont été utilisés. Un travail de titan !


## Mais il est partout !

Oui.

Putain de oui qu'il est partout cet instrument. [Classique](https://www.youtube.com/watch?v=eDFFUIGoBUc) (YT), [Jazz](https://www.youtube.com/watch?v=8ICZxjCeUW0) (YT), chanson française, musiques actuelles ou passées, France et reste du monde, qu'il soit dansé ou simplement écouté, qu'il soit utilisé pour soigner l'arthrose de pépé le dimanche après-midi ou pour célébrer une divinité d'Afrique, l'accordéon est présent depuis 3 siècles, apprécié, aimé, dans énormément de genres, cultures et lieux différents.

Donc amis mélomanes ou non, cessez d'avoir peur d'aimer cet instrument par peur de jugement de la part de personne ne connaissant pas toute l'immensité qu'il représente. Non il n'est pas limité à un seul genre musical, non il n'est pas fait uniquement pour les personnes âgées ou les jeunes poètes révolutionnaires avec le mégot au coin de la bouche.

Il est beau, polyvalent, d'une complexité mécanique et d'une précision d'accordage digne d'une horloge suisse. Le maîtriser correctement nécessite comme beaucoup d'instrument des années et des années de pratique. Il est possible d'écouter de nombreux artistes comme ceux cités dans cet article, mais encore bien d'autres qui donnent à l'accordéon tout son sens, sa beauté musicale et sa richesse. Qu'il soit en solo ou accompagné plutôt que de juger bêtement un instrument sur un seul genre musical par simple méconnaissance du sujet.

Alors, s'il vous plaît, n'ayez pas peur, faites écouter des choses aux gens qui vous expriment ces préjugés et qui depuis plusieurs dizaines années font de cet instrument le vilain petit canard aux yeux des non musiciens ou non mélomanes avertis.

Si toutefois vous souhaitez réagir sur ce sujet et partagez vos expériences ou découvertes musicales, n'hésitez pas à rejoindre les listes de diffusion (voir onglet "Communauté" en haut de la page) !